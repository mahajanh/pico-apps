#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.


/**
 * @brief LAB #02 - TEMPLATE
 *        Main entry point for the code.
 * 
 * @return int      Returns exit-status zero on completion.
 */

float calculatePiSinglePrecision() {
   
   double pi = 1.0;
   int val = 1;
   
     while (val <= 100000) 
     {

  // Calculate PI with single precision
          float numerator = 2.0f * val;
         float denom = numerator - 1.0f;
        pi = pi*((numerator / denom) * (numerator / (denom + 2.0f)));
    val++;
     }
   return  pi*2.0f ; 
}


double calculatePiDoublePrecision() {
   
   double pi = 1.0;
   int val = 1;

// Calculate PI with double precision
     while (val <= 100000) 
     {
  
    float numerator = 2.0 * val;
         float denom = numerator - 1.0;
  pi = pi*( (numerator / denom) * (numerator / (denom + 2.0)));
         val++;
   }
     return   pi*2.0; 
}



int main() {


    float single_pi = calculatePiSinglePrecision();
   printf("The calculated value of PI (Single Precision): %f\n", single_pi);
  

// Calculate approximation error for PI with single precision
   float singleError = fabs(3.14159265359 - single_pi);
   printf("The approximation error for PI (Single Precision): %f\n", singleError);
  


   double double_pi = calculatePiDoublePrecision();
   printf("The calculated value of PI (Double Precision): %f\n", double_pi);
  
  // Calculate approximation error for PI with double precision
   double doubleError = fabs(3.14159265359 - double_pi);
   printf("The approximation error for PI (Double Precision): %f\n", doubleError);


   return 0;

}